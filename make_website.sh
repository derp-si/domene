#!/bin/sh
cd domene_web/

mkdir -p out

echo
echo Analyzing data...
echo

python analyze.py

echo
echo Rendering website...
echo

python render.py

echo
echo DONE
echo
