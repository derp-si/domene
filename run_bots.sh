#!/bin/bash

cd domene_scrapy/

for bot_name in $(ls domene_scrapy/spiders/*.py); do    
    bot_name="${bot_name%.*}"
    bot_name="${bot_name##*/}"
    if [[ "$bot_name" = "__init__" ]]; then
        continue
    fi
    echo
    echo "Running $bot_name"
    echo
    scrapy crawl "$bot_name" -o "data/$bot_name.csv" -t csv --loglevel INFO
done
