# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from enum import Enum
import scrapy


class PriceType(str, Enum):
    REGISTRATION = "registration"
    RENEWAL = "renewal"
    TRANSFER = "transfer"

class Currency(str, Enum):
    EUR = "EUR"
    USD = "USD"

class DomainPrice(scrapy.Item):
    provider: str = scrapy.Field()
    tld: str = scrapy.Field()
    price_type: PriceType = scrapy.Field()
    price: float = scrapy.Field()
    price_before_var: float = scrapy.Field()
    currency: str = scrapy.Field()
