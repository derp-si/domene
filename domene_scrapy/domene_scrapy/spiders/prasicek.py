from domene_scrapy.spiders.neoserv import NeoservSpider


class PrasicekSpider(NeoservSpider):
    name = "prasicek"
    allowed_domains = ["prasicek.si"]

    cenik_base = 'https://www.prasicek.si/domene/cenik/'
