import scrapy
import scrapy.http

from domene_scrapy.items import Currency, DomainPrice, PriceType


class NeoservSpider(scrapy.Spider):
    name = "neoserv"
    allowed_domains = ["neoserv.si"]

    cenik_base = 'https://www.neoserv.si/domene/cenik-domen/' 
    categories = ('nacionalne', 'splosne', 'tematske')
    price_types = {
        PriceType.REGISTRATION: 'nova',
        PriceType.RENEWAL: 'podaljsanje',
        PriceType.TRANSFER: 'prenos'
    }

    def start_requests(self):
        for cat in self.categories:
            yield scrapy.Request(
                self.cenik_base + cat,
                cb_kwargs=dict(cat=cat),
                callback=self.parse_category,
            )

    def parse_category(self, response: scrapy.http.HtmlResponse, cat):
        for page_el in response.css('.pagination .js-ajax-change'):
            page = int(page_el.css('::text').get())
            for type in self.price_types:
                yield self.make_request(cat, type, page)

    def make_request(self, cat, type, page=1):
        return scrapy.Request(
            self.make_url(cat, type, page), 
            headers={
                'Accept': 'application/json, text/javascript, */*; q=0.01',
            },
            cb_kwargs=dict(page=page, cat=cat, type=type),
        )

    def make_url(self, cat, type, page=1):
        url = f'{self.cenik_base}{cat}?page={page}&source=ajax/domene/cenik&sourceID=domain-pricelist&stanje={self.price_types[type]}'
        return url

    def parse(self, response: scrapy.http.TextResponse, page, cat, type):
        data = response.json()
        html_str = data['data']['targets']['domain-pricelist']
        html = scrapy.http.HtmlResponse(url=response.url, body=html_str, encoding='utf-8')
        for row in html.css('.trow.listing'):
            tld = row.css('.name strong::text').get().strip(' .')
            price = float(row.css('.new-price::text').get().strip(' €').replace('.', '').replace(',', '.').strip())
            yield DomainPrice(
                provider=self.name,
                tld=tld,
                price_type=type,
                price=price,
                price_before_var=price / self.settings['VAT_MULTIPLIER'],
                currency=Currency.EUR,
            )
