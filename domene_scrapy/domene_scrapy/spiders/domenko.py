from domene_scrapy.spiders.domene_si import DomeneSISpider


class DomenkoSpider(DomeneSISpider):
    name = "domenko"
    allowed_domains = ["domenko.si"]
    start_urls = [
        'https://domenko.si/servis/cart.php?a=add&domain=register',
    ]
