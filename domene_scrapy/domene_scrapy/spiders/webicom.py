from domene_scrapy.spiders.domene_si import DomeneSISpider


class WebicomSpider(DomeneSISpider):
    name = "webicom"
    allowed_domains = ["webicom.net"]
    start_urls = [
        'https://whmcs.webicom.net/cart.php?a=add&domain=register',
    ]
