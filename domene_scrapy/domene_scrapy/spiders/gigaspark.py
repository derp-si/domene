import scrapy
import scrapy.http

from domene_scrapy.items import Currency, DomainPrice, PriceType


class GigasparkSpider(scrapy.Spider):
    name = "gigaspark"
    allowed_domains = ["gigaspark.com"]
    start_urls = [
        'https://www.gigaspark.com/domene/cenik',
    ]

    col_order = (PriceType.REGISTRATION, PriceType.RENEWAL, PriceType.TRANSFER)

    def parse(self, response: scrapy.http.HtmlResponse):
        
        for row in response.css('.tld-row:not(.no-tlds)'):
            tld_el, prices_el = row.xpath('./div')

            tld = tld_el.css('strong::text').get().strip('. ')

            for type, price_el in zip(self.col_order, prices_el.css('.col-xs-4')):
                price_txt = price_el.css('::text').get().strip()
                if not price_txt:
                    continue

                price = float(price_txt.strip(' €').replace('.', '').replace(',', '.').strip())

                yield DomainPrice(
                    provider=self.name,
                    tld=tld,
                    price_type=type,
                    price=price * self.settings['VAT_MULTIPLIER'],
                    price_before_var=price,
                    currency=Currency.EUR,
                )


