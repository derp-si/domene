import scrapy
import scrapy.http

from domene_scrapy.items import Currency, DomainPrice, PriceType


class HitrostSpider(scrapy.Spider):
    name = "hitrost"
    allowed_domains = ["hitrost.com"]
    start_urls = [
        'https://www.hitrost.com/domene/cenik-domen/',
    ]

    custom_settings = {
        'USER_AGENT': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0',
    }

    col_order = (PriceType.REGISTRATION, PriceType.RENEWAL, PriceType.TRANSFER)


    def parse(self, response: scrapy.http.HtmlResponse):
        # Skip first table because it contains duplicates
        for table in response.css('.wpb_tabs table')[1:]:
            for row in table.css('tbody tr'):
                cells = row.css('td')

                tld = cells[0].css('::text').get().strip('. ')
                price_els = cells[1:4]

                for type, price_el in zip(self.col_order, price_els):
                    price_txt = price_el.css('::text').get().strip()
                    if 'brezplač' in price_txt:
                        continue
                    price = float(price_txt.replace('EUR', '').strip().strip('\u202c').replace('.', '').replace(',', '.').strip())

                    yield DomainPrice(
                        provider=self.name,
                        tld=tld,
                        price_type=type,
                        price=price * self.settings['VAT_MULTIPLIER'],
                        price_before_var=price,
                        currency=Currency.EUR,
                    )


