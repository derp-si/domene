import scrapy
import scrapy.http

from domene_scrapy.items import Currency, DomainPrice, PriceType


class DomencaSpider(scrapy.Spider):
    name = "domenca"
    allowed_domains = ["domenca.com"]
    start_urls = [
        'https://www.domenca.com/domene/seznam-koncnic/',
    ]


    def parse(self, response: scrapy.http.HtmlResponse):
        
        for row in response.css('.domenca-domain-row'):
            tld = row.css('.domain::text').get().strip('\n .')

            price_big = "".join(row.css('.flex-grow .block').xpath('.//text()').getall()).replace('\n','').strip()

            if not price_big:
                continue

            price_strike = row.css('.line-through::text')
            if price_strike:
                price_reg = price_strike.get().strip()
            else:
                price_reg = price_big
            
            price_renew = price_big

            price_reg = float(price_reg.strip(' €').replace('.', '').replace(',', '.').strip())
            price_renew = float(price_renew.strip(' €').replace('.', '').replace(',', '.').strip())

            yield DomainPrice(
                provider=self.name,
                tld=tld,
                price_type=PriceType.REGISTRATION,
                price=price_reg,
                price_before_var=price_reg / self.settings['VAT_MULTIPLIER'],
                currency=Currency.EUR,
            )
            
            yield DomainPrice(
                provider=self.name,
                tld=tld,
                price_type=PriceType.RENEWAL,
                price=price_renew,
                price_before_var=price_renew / self.settings['VAT_MULTIPLIER'],
                currency=Currency.EUR,
            )


