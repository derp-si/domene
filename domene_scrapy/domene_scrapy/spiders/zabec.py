import scrapy
import scrapy.http

from domene_scrapy.items import Currency, DomainPrice, PriceType


class ZabecSpider(scrapy.Spider):
    name = "zabec"
    allowed_domains = ["zabec.net"]
    start_urls = [
        'https://www.zabec.net/cenik',
    ]

    custom_settings = {
        'USER_AGENT': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0',
    }


    def parse(self, response: scrapy.http.HtmlResponse):
        
        for row in response.css('#domain_table tr'):
            
            tld_el, price_el = row.css('td')

            tld_text = tld_el.css('::text').get()
            tld = tld_text.split(' ')[-1].strip('.')

            if 'Podaljšanje domene' in tld_text:
                type = PriceType.RENEWAL
            elif 'Registracija domene' in tld_text:
                type = PriceType.REGISTRATION
            elif 'Prenos domene' in tld_text:
                type = PriceType.TRANSFER
            else:
                continue

            price = float(price_el.css('::text').get().split(' €')[0].replace('.', '').replace(',', '.').strip())

            yield DomainPrice(
                provider=self.name,
                tld=tld,
                price_type=type,
                price=price,
                price_before_var=price / self.settings['VAT_MULTIPLIER'],
                currency=Currency.EUR,
            )
        

