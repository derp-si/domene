import scrapy
import scrapy.http

from domene_scrapy.items import Currency, DomainPrice, PriceType


class DomeneSISpider(scrapy.Spider):
    name = "domene_si"
    allowed_domains = ["domene.si"]
    start_urls = [
        'https://www.domene.si/cart.php?a=add&domain=register',
    ]

    custom_settings = {
        'USER_AGENT': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0',
    }

    col_order = (PriceType.REGISTRATION, PriceType.TRANSFER, PriceType.RENEWAL)


    def parse(self, response: scrapy.http.HtmlResponse):
        
        for row in response.css('.tld-row:not(.no-tlds)'):
            tld_el, prices_el = row.xpath('./div')

            tld = tld_el.css('strong::text').get().strip('. ')

            for type, price_el in zip(self.col_order, prices_el.css('.col-xs-4')):
                price_txt = price_el.css('::text').get().strip()
                if not price_txt:
                    continue

                price = float(price_txt.replace('EUR', '').strip(' €').replace('.', '').replace(',', '.').strip())

                yield DomainPrice(
                    provider=self.name,
                    tld=tld,
                    price_type=type,
                    price=price * self.settings['VAT_MULTIPLIER'],
                    price_before_var=price,
                    currency=Currency.EUR,
                )


