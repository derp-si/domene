import scrapy
import scrapy.http

from domene_scrapy.items import Currency, DomainPrice, PriceType


class HostkoSpider(scrapy.Spider):
    name = "hostko"
    allowed_domains = ["hostko.si"]
    start_urls = [
        'https://www.hostko.si/domene/',
    ]

    col_order = (PriceType.REGISTRATION, PriceType.RENEWAL, PriceType.TRANSFER)


    def parse(self, response: scrapy.http.HtmlResponse):
        for tab in response.css('.tab-pane'):
            for row in tab.css('tbody tr'):
                tld_el, reg_el, renew_el, trans_el = row.css('td')
                tld = tld_el.css('::text').get().strip(' .').lower()

                for type, price_el in zip(self.col_order, (reg_el, renew_el, trans_el)):
                    if price_el.css('.green').get():
                        price_el = price_el.css('.green')

                    price_txt = price_el.css('::text').get().strip()

                    if 'brezplač' in price_txt:
                        price_txt = "0"
                    elif '/' in price_txt:
                        continue

                    price = float(price_txt.strip(' €').replace('.', '').replace(',', '.').strip())
                    yield DomainPrice(
                        provider=self.name,
                        tld=tld,
                        price_type=type,
                        price=price,
                        price_before_var=price / self.settings['VAT_MULTIPLIER'],
                        currency=Currency.EUR,
                    )
