## Slovenian domain registrar scraper

Supported registrars:

 - Domenca
 - Domene.si
 - Domenko
 - Gogaspark
 - Hitrost.com
 - Neoserv
 - Prašiček
 - Webicom
 - Žabec

Permalinks:

 - **CSV for all providers:** https://gitlab.com/derp-si/domene/-/jobs/artifacts/main/raw/public/domene.csv?job=pages
