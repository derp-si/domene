import os
from pathlib import Path

import pandas as pd

os.chdir(Path(__file__).parent)
csv_files = Path('../domene_scrapy/data/').glob('*.csv')

dfs = []
for f in csv_files:
    try:
        df = pd.read_csv(f)
        dfs.append(df)
    except pd.errors.EmptyDataError:
        pass

df = pd.concat(dfs)
df.price = df.price.round(2)

# Find duplicates
duplicates = df[df.duplicated(subset=['provider', 'tld', 'price_type'], keep=False)]
print("Duplicates:")
print(duplicates.groupby('provider').size())

# Pivot to join renewal, registration and transfer prices
pivoted = df.pivot_table(
    index=['provider', 'tld'],
    columns='price_type',
    values='price',
    aggfunc='first',
)
pivoted.reset_index(inplace=True)
pivoted.columns.name = None

# Save to csv
pivoted.to_csv('out/domene.csv', index=False)

# Find cheapest renewal, registration and transfer prices
cheapest_renewal = df[df.price_type == 'renewal'][['tld', 'provider', 'price']].sort_values('price').groupby('tld').first()
cheapest_registration = df[df.price_type == 'registration'][['tld', 'provider', 'price']].sort_values('price').groupby('tld').first()
cheapest_transfer = df[df.price_type == 'transfer'][['tld', 'provider', 'price']].sort_values('price').groupby('tld').first()

# Merge
merged = cheapest_renewal.merge(
    cheapest_registration,
    left_index=True,
    right_index=True,
    suffixes=('_renewal', '_registration'),
)
merged = merged.merge(
    cheapest_transfer,
    left_index=True,
    right_index=True,
)
merged.rename(columns={'price': 'price_transfer', 'provider': 'provider_transfer'}, inplace=True)

# Save to csv
merged.to_csv('out/domene_cheapest.csv')