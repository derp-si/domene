import csv
from pathlib import Path

import jinja2

OUT_PATH = Path(__file__).parent / 'out'

env = jinja2.Environment(
    loader=jinja2.FileSystemLoader(Path(__file__).parent / 'templates'),
    autoescape=jinja2.select_autoescape(['html', 'xml'])
)


def render(in_file, out_file, **kwargs):
    template = env.get_template(in_file)
    rendered = template.render(**kwargs)
    with open(OUT_PATH / out_file, 'w') as f:
        f.write(rendered)

with open(OUT_PATH / 'domene_cheapest.csv') as f:
    reader = csv.DictReader(f)
    rows = list(reader)

render('index.html', 'index.html', table=rows)


